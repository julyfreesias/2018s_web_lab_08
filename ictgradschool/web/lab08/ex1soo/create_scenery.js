$(document).ready(function () {
    // TODO: Your code here


    $("#choose-background input").on("click", function () {
        var subbg = $("#choose-background input:checked").attr('id');
        var subbg = subbg.substring(6, subbg.length);

        var typeOfFile = ".jpg"
        if ("background6" === subbg) {
            typeOfFile = ".gif"
        }

        $("#background").attr("src", "../images/" + subbg + typeOfFile)

    });


    //need to use change rather than the click event only
    $("#toggle-components").change(function () {           //page read all event including click,transforming..etc
        $("#toggle-components input").each(function () { //but we still need 'input' here cause we gonna read all input from each clicking
            var df = $(this).attr('id');                // use the each element as 'this' from previous line and find attribute with'id'
            var subdf = df.substring(6, df.length);     //now we substring to get exact ID name that is match to img name.
            if ($(this).is(':checked')) {               //when we using 'this', 'is' should be followed by
                $('#' + subdf).show();                  // when using variable name, we should put '#'+ before
            } else {
                $('#' + subdf).hide();
            }
        });
    }).change();

    var xPosition = 0;
    var yPosition = 0;
    var size=1;
    function updateDolphins() {
        $(".dolphin").css("transform", "scale(" + size + "," + size + ") translateX(" + xPosition + "px) translateY(" + yPosition + "px)");
    }
    $("#vertical-control").on('input change', function () {
            console.log($(this).val());
            yPosition = $(this).val();
            // $(".dolphin").css("transform", "scale(" + size + "," + size + ") translateX(" + xPosition + "px) translateY(" + yPosition + "px)");

            updateDolphins();
        }
    );

    $("#size-control").on('input change', function () {
            console.log($(this).val());
            size = 1 + $(this).val() / 100;

            updateDolphins();

        }
    );

    $("#horizontal-control").on('input change', function () {
            console.log($(this).val());
            xPosition = $(this).val();


            updateDolphins();

        }
    );


    //  // // size and position of dophin img
    //  //image slide
    //
    // $(function(){
    //     var vS, size, hS;
    // }
    // $("input[type='range']").change(function()){
    //
    //
    //  }
    //
    //  $("#adjust-components").change(function(){
    //      $("#adjust-components input").on("click",(function(){
    //          var df = $(this).attr('id');
    //
    //          var vS = $("vertical-control");
    //          var size= $("size-control");
    //          var hS= $("horizontal-control");
    //
    //          vS.on('click',function (e) { goto('next');}
    //
    //          })
    //
    //
    //
    //
    //
    //
    //      })});

});
